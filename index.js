const express = require('express');
const app = express();

const dotenv = require('dotenv');
dotenv.config();

const portnum = process.env.NODE_LOCAL_PORT || 9090

app.get('/api', (req, res) => {
    res.json({data: 'Result from port ' + portnum + '!'});
});

app.listen(portnum, () => {
    console.log('App listening on port ' + portnum + '!');
});
