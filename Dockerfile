FROM node:16.17.0-bullseye-slim

ENV NODE_LOCAL_PORT=

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . . 

CMD ["node", "index.js"]
